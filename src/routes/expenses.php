<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

include '../ChromePhp.php';

$app = new Slim\App;

// Fetch All
$app->get('/api/expenses/{rezerv}', function (Request $request, Response $response) {

    $rezerv = $request->getAttribute('rezerv');

    $sql = "SELECT * FROM expenses WHERE rezerv = ?";

    try {
        $db = new db();
        $db = $db->connect();


        $stmt = $db->prepare($sql);
        $stmt->execute([$rezerv]);
        $responseData = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $response->withJson($responseData);

    } catch (PDOException $e) {
        $e->getMessage();
    }

});

// Fetch By id

$app->get('/api/expenses/{rezerv}/{id}', function (Request $request, Response $response) {
    //SELECT * FROM expenses WHERE rezerv = 101270801987535879208 AND expenseid = 55"
    $rezerv = $request->getAttribute('rezerv');
    $id = $request->getAttribute('id');

    $sql = "SELECT * FROM expenses WHERE rezerv = ? AND expenseid = ?";

    try {
        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sql);
        $stmt->execute([$rezerv, $id]);
        $expense = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        echo json_encode($expense);

    } catch (PDOException $e) {
        $e->getMessage();
    }

});


// Add Expense

$app->post('/api/expenses/{rezerv}/add', function (Request $request, Response $response) {
    $rezerv = $request->getAttribute('rezerv');
    $description = $request->getParam('description');
    $place = $request->getParam('place');
    $variety = $request->getParam('variety');
    $isJoint = $request->getParam('isJoint');
    $amount = $request->getParam('amount');


    $sql = "INSERT INTO expenses (description, place, variety, isJoint, amount, rezerv) VALUES
        (:description, :place, :variety, :isJoint, :amount, :rezerv)";

    try {
        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sql);

        $stmt->bindParam(':description', $description);
        $stmt->bindParam(':place', $place);
        $stmt->bindParam(':variety', $variety);
        $stmt->bindParam(':isJoint', $isJoint);
        $stmt->bindParam(':amount', $amount);
        $stmt->bindParam(':rezerv', $rezerv);

        $stmt->execute();

        echo "{Expense added}";

    } catch (PDOException $e) {
        echo $e->getMessage();
    }

});

// Update Expense

$app->put('/api/expenses/{rezerv}/update/{id}', function (Request $request, Response $response) {
// print_r($request->getParsedBody());
    $requestArr = $request->getParsedBody();

    $rezerv = $request->getAttribute('rezerv');
    $id = $request->getAttribute('id');
    $description = $requestArr['description'];
//    echo $description;
    $place = $requestArr['place'];
    $variety = $requestArr['variety'];
    $isJoint = $requestArr['isJoint'];
    $amount = $requestArr['amount'];

    //UPDATE expenses SET `description`='$description' , `variety`='$variety' ,
    // `place`='$place' , `isJoint`='$isJoint', `amount`='$amount' WHERE expenseid=$id

    $sql = "UPDATE expenses SET
        description = :description,
        variety = :variety,
        place = :place,
        isJoint = :isJoint,
        amount = :amount
        WHERE expenseid = :id";

    try {
        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sql);

        $stmt->bindParam(':description', $description);
        $stmt->bindParam(':variety', $variety);
        $stmt->bindParam(':place', $place);
        $stmt->bindParam(':isJoint', $isJoint);
        $stmt->bindParam(':amount', $amount);
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        echo "{Expense updated}";

    } catch (PDOException $e) {
        echo $e->getMessage();
    }

});

// Delete expense

$app->delete('/api/expenses/{rezerv}/delete/{id}', function (Request $request, Response $response) {

    $rezerv = $request->getAttribute('rezerv');
    $id = $request->getAttribute('id');

    // DELETE FROM `expenses` WHERE `expenses`.`expenseid` = $id
    $sql = "DELETE FROM expenses WHERE rezerv = $rezerv AND expenseid = $id";

    try {
        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sql);
        $stmt->execute();
        $db = null;

        echo "Expense Deleted";

    } catch (PDOException $e) {
        $e->getMessage();
    }

});

//-----------------------------------//\\-------------------------\\

// Get participants
$app->get('/api/participants/{expenseid}', function (Request $request, Response $response) {

    $expenseid = $request->getAttribute('expenseid');

    $sql = "SELECT * FROM participants WHERE expenseid = ?";

    try {
        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sql);
        $stmt->execute([$expenseid]);
        $responseData = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $response->withJson($responseData);

    } catch (PDOException $e) {
        $e->getMessage();
    }
//    echo "expenses";

});

// Get participant by id
$app->get('/api/participantsById/{participantid}', function (Request $request, Response $response) {

    $participantid = $request->getAttribute('participantid');

    $sql = "SELECT * FROM participants WHERE participantid = ?";

    try {
        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sql);
        $stmt->execute([$participantid]);
        $responseData = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $response->withJson($responseData);

    } catch (PDOException $e) {
        $e->getMessage();
    }
//    echo "expenses";

});

// add participant to expense
$app->post('/api/participants/{expenseid}/add', function (Request $request, Response $response) {
    $expenseid = $request->getAttribute('expenseid');
    $description = $request->getParam('description');
    $name = $request->getParam('name');
    $amountOfCut = $request->getParam('amountOfCut');
    $isJoint = $request->getParam('isJoint');
    $isPaid = $request->getParam('isPaid');
    // FIXME BURALARDA BİYERDE HATA


    if ($amountOfCut > 0) {
        // do nothing
    } else {
        // rakam olarak tutunca sorun cikarmisti
        $amountOfCut = "0";
    }


    $isPaid = $request->getParam('isPaid');
    $amountOfCut = $request->getParam('amountOfCut');
    $expenseid = $request->getParam('expenseid');


    $sql = "INSERT INTO participants (name, description, isJoint, amountOfCut, isPaid, expenseid) VALUES
        (:name, :description, :isJoint, :amountOfCut, :isPaid, :expenseid)";

    try {
        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sql);

        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':description', $description);
        $stmt->bindParam(':isJoint', $isJoint);
        //bos stringi php bosluk goruyor cok sacma null da değil
        $stmt->bindParam(':amountOfCut', floatval($amountOfCut));
        $stmt->bindParam(':isPaid', $isPaid);
        $stmt->bindParam(':expenseid', $expenseid);

        $stmt->execute();

        echo "{Participant added}";

    } catch (PDOException $e) {
        echo $e->getMessage();
    }

});

// update participant
$app->put('/api/participants/update/{participantid}', function (Request $request, Response $response) {
    $requestArr = $request->getParsedBody();
    echo $requestArr['isJoint'];
    $participantid = $request->getAttribute('participantid');
    $name = $requestArr['name'];
    $description = $requestArr['description'];
    $amountOfJoint = $requestArr['amountOfJoint'];
    $isJoint = $requestArr['isJoint'];
    $isPaid = $requestArr['isPaid'];
    $amountOfCut = $requestArr['amountOfCut'];

    // TODO arraydan alıyor ok ama boş kalan kısımlarda hata var gibi ayrıca amount of Joint e gerek yok sanki

    $sql = "UPDATE participants SET name = :name, description = :description, isJoint = :isJoint, amountOfJoint = :amountOfJoint,
        amountOfCut = :amountOfCut, isPaid = :isPaid WHERE participantid = :participantid";

    try {
        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sql);

        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':description', $description);
        $stmt->bindParam(':isJoint', $isJoint);
        $stmt->bindParam(':amountOfJoint', $amountOfJoint);
                                                        // Bos gonderince SQL hatası veriyodu
        $stmt->bindParam(':amountOfCut', floatval($amountOfCut));
        $stmt->bindParam(':isPaid', $isPaid);
        $stmt->bindParam(':participantid', $participantid);

        $stmt->execute();

        echo "{Participant added}";

    } catch (PDOException $e) {
        echo $e->getMessage();
    }

});

// DELETE PARTICIPANT
$app->delete('/api/participants/delete/{id}', function (Request $request, Response $response) {

    $id = $request->getAttribute('id');

    // DELETE FROM `expenses` WHERE `expenses`.`expenseid` = $id
    $sql = "DELETE FROM participants WHERE participantid = $id";

    try {
        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sql);
        $stmt->execute();
        $db = null;

        echo "Participant Deleted";

    } catch (PDOException $e) {
        $e->getMessage();
    }

});